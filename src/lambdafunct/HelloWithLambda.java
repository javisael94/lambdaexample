package lambdafunct;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class HelloWithLambda {
	

	public void test_ordenacion() {
		
		List<String> nombres =  Arrays.asList("Javier","Car","Alejandro","Asamanche");
		
		Comparator<String>  comparador = (o1 , o2) ->   o2.length() - o1.length();
		
		Collections.shuffle(nombres);
		Collections.sort(nombres, comparador);
		
		
		
		for(String n : nombres) {
			System.out.println(n);
		}
	}

	public static void main(String[] args) {

		HelloWithLambda h = new HelloWithLambda();
		h.test_ordenacion();
		
	}

}
