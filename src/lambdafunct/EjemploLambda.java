package lambdafunct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class EjemploLambda {

	
	public static void main(String[] args) {
		
		ArrayList<Integer> mayores = (ArrayList<Integer>)Arrays
				.asList(1,2,3,4,5,6,7,8,9)
				.stream()
				.filter(x -> x>5 && x % 2 == 1)
				.collect(Collectors.toCollection(() -> new ArrayList<Integer>()));
		
		mayores.forEach(e -> System.out.println(e));
			
		
	}
}
